import * as http from 'http';
import * as debug from 'debug';
import App from './application/App';

/**
 * [Server it actually initlize express app module to node js server]
 */
class Server {
    
    /**
     * [server It keep http server instance]
     * @type {Http.Server}
     */
    private server: any;

    /**
     * [port It keeps port to connect]
     * @type {Number}
     */
    private port: number;

    /**
     * [address It keeps the address to connect]
     * @type {String}
     */
    private address: string;

    /**
     * [app It contains the App module]
     * @type {Any}
     */
    private app: any;

    /**
     * [constructor]
     * @param  {Any} app: any [The app module]
     * @return {void}
     */
    constructor(app: any) {
        this.app = app;
        this.setPort();
        this.connect();
    }

    /**
     * [setPort can set the port by normalizing it]
     * @return {void}
     */
    setPort(): any {
        let val = process.env.PORT || 3000;
        let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;


        if (isNaN(port)) {
            return false;
        }

        if (port < 0) {
            return false;
        }

        this.port = port;
        //this.app.set('port', this.port);
    }

    /**
     * [connect actually create an instancece of http server and connct to server]
     * @return {void}
     */
    connect(): void {
        this.server = http.createServer(this.app);
        this.server.listen(this.port);
        this.server.on('error', this.onError.bind(this));
        this.server.on('listening', this.onListening.bind(this));
    }

    /**
     * [onError execute in case of any error in connection]
     * [error is a instance of exception]
     * @type {void}
     */
    onError(error: NodeJS.ErrnoException): void {
        if (error.syscall !== 'listen') throw error;
        let bind = (typeof this.port === 'string') ? 'Pipe ' + this.port : 'Port ' + this.port;
        switch (error.code) {
            case 'EACCES':
                console.error(`${bind} requires elevated privileges`);
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(`${bind} is already in use`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * [onLisening will trigger every time it receives an http request from client]
     * @type {void}
     */
    onListening(): void {
        let addr: string = this.server.address();
        let port: number = this.port;
        let bind: string = (typeof addr === 'string') ? `pipe ${addr}` : 'port ${addr.port}';
        debug(`Listening on ${bind}`);
    }

}

new Server(App);