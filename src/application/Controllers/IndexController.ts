import { iController } from 'core/interfaces';
import { Router, Request, Response } from 'express';
import { BaseController } from 'core';

/**
 * [IndexController description]
 * @type {[type]}
 */
class IndexController implements iController
{
	/**
	 * [index description]
	 * @param {Request}  req  [description]
	 * @param {Response} res  [description]
	 * @param {Function} next [description]
	 */
	public index(req: Request, res: Response, next: Function): void
	{
		res.json({action: 'index action for index'});
	}
};

export default IndexController;