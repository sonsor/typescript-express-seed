import { Router, Request, Response } from 'express';
import { BaseController } from 'core';
import { iController } from 'core/interfaces';

/**
 * [BaseController description]
 * @type {[type]}
 */
class UserController extends BaseController implements iController
{
	/**
	 * [index description]
	 * @param  {Request}    req  [description]
	 * @param  {Response}   res  [description]
	 * @param  {Function}   next [description]
	 * @return {Promise<T>}      [description]
	 */
	public async index(req: Request, res: Response, next: Function): Promise<T>
	{
		let data = await this.getService().get();
		res.json(data);
	}

	/**
	 * [auth description]
	 * @param {Request}  req  [description]
	 * @param {Response} res  [description]
	 * @param {Function} next [description]
	 */
	public auth(req: Request, res: Response, next: Function): void
	{
		console.log('user authentication');
		next();
	}
};

export default UserController;