import { iControllerFactory, iServiceLocator } from 'core/interfaces';
import UserController from './UserController';

/**
 * [UserControllerFactory description]
 * @type {[type]}
 */
class UserControllerFactory implements iControllerFactory
{
	/**
	 * [create description]
	 * @param  {any}            serviceLocator [description]
	 * @return {UserController}                [description]
	 */
	public static create(serviceLocator: iServiceLocator): UserController
	{
		let obj = new UserController();
		obj.setService(serviceLocator.get('User'));
		return obj;
	}
}

export default UserControllerFactory;