import { iControllerFactory, iServiceLocator } from 'core/interfaces';
import IndexController from './IndexController';

/**
 * [IndexControllerFactory description]
 * @type {[type]}
 */
class IndexControllerFactory implements iControllerFactory
{
	/**
	 * [create description]
	 * @return {IndexController} [description]
	 */
	public static create(serviceLocator: iServiceLocator): IndexController
	{
		let obj = new IndexController();
		return obj;
	}
}

export default IndexControllerFactory;