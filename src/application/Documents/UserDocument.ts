import { iDocument } from 'core/interfaces';
import { prop, pre } from 'typegoose';
import { BaseDocument } from 'core';

/**
 * [User description]
 * @type {[type]}
 */
class User extends BaseDocument implements iDocument
{
  @prop()
  public name?: string;

  @prop()
  public email?: string;

  @prop()
  public password?: string;

  @prop()
  public username?: string;
}
 
const UserDocument = new User().getModel(User);
export default UserDocument;

