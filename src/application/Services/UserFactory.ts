import User from './User';
import { DocumentManager } from 'core';

/**
 * [UserFactory description]
 * @type {[type]}
 */
class UserFactory
{
	/**
	 * [create description]
	 * @param {any} documentManager [description]
	 */
	public create(documentManager: any)
	{
		let obj: User = new User();
		obj.setDocumentManager(DocumentManager);
		return obj;
	}
}

export default new UserFactory();