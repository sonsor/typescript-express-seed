import { iService } from 'core/interfaces';
import { Typegoose } from 'typegoose';
import { BaseService } from 'core';

/**
 * [BaseService description]
 * @type {[type]}
 */
class User extends BaseService implements iService
{
	/**
	 * [get description]
	 * @return {Promise<Typegoose>} [description]
	 */
	async get(): Promise<Typegoose>
	{
		let obj: any = this.getDocumentManager().getRepository('User');
		let users: any = await obj.find({}).exec();
		return users;
	}

	/**
	 * [insert description]
	 * @return {Promise<Typegoose>} [description]
	 */
	async insert(): Promise<Typegoose>
	{
		let newUser = new this.getDocumentManager().getRepository('User')({
              email: "wasif0332@g.com",
              name: "wasif farooq",
              password: "Admin123!!",
              username: "wasif0332"
          });
          return await newUser.save();
	}
}

export default User;