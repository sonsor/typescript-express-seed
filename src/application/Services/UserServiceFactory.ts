import { iServiceFactory, iDocumentManager } from 'core/interfaces';
import User from './UserService';
import { DocumentManager } from 'core';

/**
 * [iServiceFactory description]
 * @type {[type]}
 */
class UserServiceFactory implements iServiceFactory
{
	/**
	 * [create description]
	 * @param  {iDocumentManager} documentManager [description]
	 * @return {iService}                         [description]
	 */
	public create(documentManager: iDocumentManager): iService
	{
		let obj: User = new User();
		obj.setDocumentManager(DocumentManager);
		return obj;
	}
}

export default new UserFactory();