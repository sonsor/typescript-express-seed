import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import { AppRouter } from 'core';

/**
 * This class can create a new express application that will provided to server
 */
class App {

    /**
     * [app it keep the express application]
     * @type {express.Application}
     */
    public app: express.Application;

    /**
     * [constructor the App class constructor]
     * @return {App} [return the new instance of App class]
     */
    constructor() {
        this.app = express();
        this.middlewares();
    }

    /**
     * [getApp getter for app property]
     * @return {express.Application} [return the express.Application instance]
     */
    getApp(): express.Application {
        return this.app;
    }

    /**
     * [create a static method to create new express application]
     * @return {express.Application} [return the express.Application instance]
     */
    static create(): express.Application {
        return new App().getApp();
    }

    /**
     * [middlewares sets the middleware that a nessary for application]
     * @return {void}
     */
    middlewares(): void {
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
        this.app.use(AppRouter.getInstance()); 
    }
}

export default App.create();