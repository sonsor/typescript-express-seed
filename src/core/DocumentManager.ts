import {iLoader, iDocumentManager } from 'core/interfaces';
import Loader from './Loader';

/**
 * [iDocumentManager description]
 * @type {[type]}
 */
class DocumentManager implements iDocumentManager
{
	/**
	 * [loader description]
	 * @type {iLoader}
	 */
	private loader: iLoader;

	/**
	 * [constructor description]
	 * @param {Loader} loader [description]
	 */
	constructor(loader: iLoader)
	{
		this.loader = loader;
	}

	/**
	 * [getRepository description]
	 * @param  {string}    name [description]
	 * @return {iDocument}      [description]
	 */
	getRepository(name: string): iDocument
	{
		try {
			let document = this.loader.getDocument(name);
			return document;
		} catch (e: any) {
			console.log(e);
		}
	}
}

export default new DocumentManager(Loader);