import { iLoader } from './iLoader';
import { iDocument } from './iDocument';

/**
 * [iDocumentManager description]
 * @type {[type]}
 */
interface iDocumentManager
{
	/**
	 * [loader description]
	 * @type {iLoader}
	 */
	private loader: iLoader;

	/**
	 * [getRepository description]
	 * @param  {string}    name [description]
	 * @return {iDocument}      [description]
	 */
	public getRepository(name: string): iDocument
}

export { iDocumentManager }