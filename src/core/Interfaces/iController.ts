import { iService } from './iService';

/**
 * [iController description]
 * @type {[type]}
 */
interface iController
{
	/**
	 * [service description]
	 * @type {iService}
	 */
	private service: iService;

	/**
	 * [setService description]
	 * @param {iService} service [description]
	 */
	public setService(service: iService): void;

	/**
	 * [getService description]
	 * @return {iService} [description]
	 */
	public getService(): iService;
}

export { iController }