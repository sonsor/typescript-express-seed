import { iDocumentManager } from './iDocumentManager';
import { iObject } from './iObject';
import { Typegoose } from 'typegoose';

/**
 * [iService description]
 * @type {[type]}
 */
interface iService
{
	/**
	 * [_dm description]
	 * @type {iDocumentManager}
	 */
	private _dm: iDocumentManager;

	/**
	 * [setDocumentManager description]
	 * @param {iDocumentManager} dm [description]
	 */
	public setDocumentManager(dm: iDocumentManager): void;

	/**
	 * [getDocumentManager description]
	 * @return {iDocumentManager} [description]
	 */
	public getDocumentManager(): iDocumentManager;

	/**
	 * [find description]
	 * @param  {iObject         = {}}        filters [description]
	 * @param  {iObject         = {}}        fields  [description]
	 * @param  {iObject         = {}}        sort    [description]
	 * @param  {number          = 0}           limit   [description]
	 * @param  {number          = 0}           skip    [description]
	 * @return {Promise<Typegoose>}   [description]
	 */
	public async find(filters: iObject = {}, fields: iObject = {}, sort: iObject = {}, limit: number = 0, skip: number = 0): Promise<Typegoose>;

	/**
	 * [edit description]
	 * @param {iObject} data [description]
	 */
	public edit(data: iObject): void;
}

export { iService }