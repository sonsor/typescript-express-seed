import { iServiceLocator } from './iServiceLocator';
import { iController } from './iController';

/**
 * [iControllerFactory description]
 * @type {[type]}
 */
interface iControllerFactory
{
	/**
	 * [create description]
	 * @param  {iServiceLocator} serviceLocator [description]
	 * @return {iController}                    [description]
	 */
	public static create(serviceLocator: iServiceLocator): iController;
}

export { iControllerFactory }