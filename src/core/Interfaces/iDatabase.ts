interface iDatabase
{
	/**
	 * [instance description]
	 * @type {iDatabase}
	 */
	static instance: iDatabase;

	/**
	 * [connection description]
	 * @type {any}
	 */
	static connection: any;

	/**
	 * [uri description]
	 * @type {string}
	 */
	private uri: string;

	/**
	 * [getInstance description]
	 * @return {any} [description]
	 */
	static getInstance(): any;;
}

export { iDatabase }