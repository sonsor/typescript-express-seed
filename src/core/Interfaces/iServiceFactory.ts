impor { iDocumentManager } from './iDocumentManager';
impor { iService } from './iService';

/**
 * [iServiceFactory description]
 * @type {[type]}
 */
interface iServiceFactory
{
	/**
	 * [create description]
	 * @param  {iDocumentManager} documentManager [description]
	 * @return {iService}                         [description]
	 */
	public create(documentManager: iDocumentManager): iService;
}

export { iServiceFactory }