import { Router } from 'express';
import { iLoader } from './iLoader';
import { iServiceLocator } from './iServiceLocator';
import { iRoute } from './iRoute';

/**
 * [iRouter description]
 * @type {[type]}
 */
interface iRouter
{
	/**
	 * [routes description]
	 * @type {Array<iRoute>}
	 */
	private routes: Array<iRoute>;

	/**
	 * [router description]
	 * @type {Router}
	 */
	private router: Router;

	/**
	 * [instance description]
	 * @type {iRouter}
	 */
	static instance: iRouter;

	/**
	 * [loader description]
	 * @type {iLoader}
	 */
	private loader: iLoader;

	/**
	 * [ServiceLocator description]
	 * @type {iServiceLocator}
	 */
	private ServiceLocator: iServiceLocator;

	/**
	 * [setup description]
	 * @param {Array<iRoute>} routes [description]
	 * @param {string     =      ''}          parent [description]
	 */
	private setup(routes: Array<iRoute>, parent: string = ''): void;

	/**
	 * [addRoute description]
	 * @param {string}          path       [description]
	 * @param {string}          method     [description]
	 * @param {Function}        action     [description]
	 * @param {Array<Function>} middlwares [description]
	 */
	private addRoute(path: string, method: string, action: Function, middlwares: Array<Function>): void;

	/**
	 * [middlewares description]
	 * @param  {iRoute}          route [description]
	 * @return {Array<Function>}       [description]
	 */
	private middlewares(route: iRoute): Array<Function>

	/**
	 * [getInstance description]
	 * @return {iRouter} [description]
	 */
	public getInstance(): iRouter;

}

export { iRouter }