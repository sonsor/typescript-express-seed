/**
 * [iRoute description]
 * @type {[type]}
 */
interface iRoute
{
	/**
	 * [path description]
	 * @type {string}
	 */
	path: string;

	/**
	 * [method description]
	 * @type {string}
	 */
	method: string;

	/**
	 * [controller description]
	 * @type {string}
	 */
	controller: string;

	/**
	 * [action description]
	 * @type {string}
	 */
	action: string;

	/**
	 * [middlewares description]
	 * @type {Array<string>}
	 */
	middlewares: Array<string>;

	/**
	 * [children description]
	 * @type {Array<iRoute>}
	 */
	children: Array<iRoute>
}

export { iRoute }