import { iLoader } from './iLoader';
import { iDocumentManaer } from './iDocumentManager';
import { iService } from './iService';

/**
 * [iServiceLocator description]
 * @type {[type]}
 */
interface iServiceLocator
{
	/**
	 * [loader description]
	 * @type {iLoader}
	 */
	private loader: iLoader;

	/**
	 * [documentManager description]
	 * @type {iDocumentManager}
	 */
	private documentManager: iDocumentManager;

	/**
	 * [get description]
	 * @param  {string}   name [description]
	 * @return {iService}      [description]
	 */
	public get(name: string): iService
}

eport { iServiceLocator }