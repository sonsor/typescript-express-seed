import { iRoute } from './iRoute';
import { iControllerFactory } from './iControllerFactory';
import { iServiceFactory } from './iServiceFactory';
import { iDocument } from './iDocument';

/**
 * [iLoader description]
 * @type {[type]}
 */
interface iLoader
{
	/**
	 * [CONTROLLERS_PATH description]
	 * @type {string}
	 */
	private CONTROLLERS_PATH: string;

	/**
	 * [SERVICES_PATH description]
	 * @type {string}
	 */
	private SERVICES_PATH: string;

	/**
	 * [DOCUMENTS_PATH description]
	 * @type {string}
	 */
	private DOCUMENTS_PATH: string;

	/**
	 * [MIDDILEWARE_PATH description]
	 * @type {string}
	 */
	private MIDDILEWARE_PATH: string;

	/**
	 * [load description]
	 * @param  {string}             type [description]
	 * @param  {string}             name [description]
	 * @return {iControllerFactory, iServiceFActory, iDocument, Function}      [description]
	 */
	public load(type: string, name: string): iControllerFactory & iServiceFactory & iDocument & Function;

	/**
	 * [getController description]
	 * @param  {string}             name [description]
	 * @return {iControllerFactory}      [description]
	 */
	public getController(name: string): iControllerFactory;

	/**
	 * [getService description]
	 * @param  {string}          name [description]
	 * @return {iServiceFactory}      [description]
	 */
	public getService(name: string): iServiceFactory;

	/**
	 * [getDocument description]
	 * @param  {string}    name [description]
	 * @return {iDocument}      [description]
	 */
	public getDocument(name: string): iDocument;

	/**
	 * [getMiddleware description]
	 * @param  {string}   name [description]
	 * @return {Function}      [description]
	 */
	public getMiddleware(name: string): Function

	/**
	 * [parse description]
	 * @param  {string} name [description]
	 * @param  {string} type [description]
	 * @return {string}      [description]
	 */
	private parse(name: string, type: string): string;

	/**
	 * [getRoutes description]
	 * @return {Array<iRoute>} [description]
	 */
	public getRoutes(): Array<iRoute>

	/**
	 * [getFiles description]
	 * @param  {string}   dir        [description]
	 * @param  {Array}    filelist   [description]
	 * @param  {string}   fileName   [description]
	 * @return {Array <string>}               [description]
	 */
	private getFiles(dir: string, filelist: Array, fileName: string): Array <string>

}

expor { iLoader }