import * as path from 'path';
const ENV = require(path.resolve(path.join(path.dirname(require.main.filename), 'constants')));

import Database from './Database';
import BaseController from './Base/BaseController';
import BaseService from './Base/BaseService';
import BaseDocument from './Base/BaseDocument';
import Loader from './Loader';
import DocumentManager from './DocumentManager';
import AppRouter from './Router';


export {
	AppRouter, 
	Loader,
	DocumentManager,
	Database, 
	BaseController, 
	BaseService, 
	BaseDocument,
	ENV
};