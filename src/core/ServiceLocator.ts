import { iServiceLocator, iLoader, iDocumentManager } from 'core/interfaces';
import Loader from './Loader';
import DocumentManager from './DocumentManager';

/**
 * [ServiceLocator description]
 * @type {[type]}
 */
class ServiceLocator implements iServiceLocator
{
	/**
	 * [loader description]
	 * @type {any}
	 */
	private loader: iLoader;

	/**
	 * [documentManager description]
	 * @type {any}
	 */
	private documentManager: iDocumentManager;

	/**
	 * [constructor description]
	 * @param {any} loader          [description]
	 * @param {any} documentManager [description]
	 */
	constructor(loader: iLoader, documentManager: iDocumentManager)
	{
		this.loader = loader;
		this.documentManager = documentManager;
	}

	/**
	 * [get description]
	 * @param  {string} name [description]
	 * @return {any}         [description]
	 */
	public get(name: string): iService
	{
		try {
			let obj = this.loader.getService(name).create(this.documentManager);
			return obj;
		} catch (e: any) {
			console.log(e.getMessage);
		}
	}
}

export default new ServiceLocator(Loader, DocumentManager);;