import { iController } from './Interfaces/iController';
import { iControllerFactory } from './Interfaces/iControllerFactory';

import { iDatabase } from './Interfaces/iDatabase';
import { iDocument } from './Interfaces/iDocument';
import { iDocumentManager } from './Interfaces/iDocumentManager';

import { iLoader } from './Interfaces/iLoader';
import { iRoute } from './Interfaces/iRoute';
import { iRouter } from './Interfaces/iRouter';
import { iObject } from './Interfaces/iObject';

import { iService } from './Interfaces/iService';
import { iServiceFactory } from './Interfaces/iServiceFactory';
import { iServiceLocator } from './Interfaces/iServiceLocator';


export {
	iController,
	iControllerFactory,
	iDatabase,
	iDocument,
	iDocumentManager,
	iLoader,
	iRoute,
	iRouter,
	iObject,
	iService,
	iServiceFactory,
	iServiceLocator
}