import { iDatabase } from 'core/interfaces';
import * as mongoose from 'mongoose';
import { ENV } from 'core';

/**
 * [Database description]
 * @type {[type]}
 */
class Database implements iDatabase
{
	/**
	 * [instance description]
	 * @type {any}
	 */
	static instance: any;

	/**
	 * [connection description]
	 * @type {any}
	 */
	static connection: any;

	/**
	 * [uri description]
	 * @type {string}
	 */
	private uri: string = '';

	/**
	 * [constructor description]
	 * @param {string} uri [description]
	 */
	constructor(uri: string)
	{

		this.uri = uri;

		mongoose.connect(this.uri).then(() => {
			console.log('[*] Connected to database');
			Database.instance = mongoose;
		}).catch((err: any) => {
			throw err;
		});;
	}

	/**
	 * [getInstance description]
	 * @return {any} [description]
	 */
	static getInstance(): any
	{
		if (this.instance && this.instance.connection.readyState) {
			return this.instance;
		}

		new Database(ENV.DB_URL);
		return mongoose;
	}
}

export default Database;