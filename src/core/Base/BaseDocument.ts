import { iDocument, iObject } from 'core/interfaces';
import { prop, pre, Typegoose, ModelType, InstanceType } from 'typegoose';
import db from '../Database';
/*
@pre<BaseDocument>('save', function(next) {
  this.createdBy = 'wasif';
  this.updatedBy = 'wasif';
  this.createdOn = new Date();
  this.updatedOn = new Date();
  
  console.log("setting created by");
  next();
})*/

/**
 * [iDocument description]
 * @type {[type]}
 */
class BaseDocument extends Typegoose implements iDocument
{
	@prop()
	public createdOn?: Date;

	@prop()
	public updatedOn?: Date;

	@prop()
	public createdBy?: string;

	@prop()
	public updatedBy?: string;

	@prop()
	public deleted?: boolean;

	@prop()
	public active?: boolean;

	/**
	 * [getModel description]
	 * @param  {any}        model [description]
	 * @param  {iObject =     {}}        options [description]
	 * @return {any}              [description]
	 */
	getModel(model: any, options: iObject = {}): any
	{
		options.existingMongoose = db.getInstance();
		return super.getModelForClass(model, options);
	}
}

export default BaseDocument;