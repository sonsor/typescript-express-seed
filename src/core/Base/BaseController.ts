import { iController, iService } from 'core/interfaces';
/**
 * [BaseController description]
 * @type {[type]}
 */
abstract class BaseController implements iController
{
	/**
	 * [service description]
	 * @type {iService}
	 */
	private service: iService;

	/**
	 * [setService description]
	 * @param {iService} service [description]
	 */
	public setService(service: iService): void
	{
		this.service = service;
	}

	/**
	 * [getService description]
	 * @return {iService} [description]
	 */
	public getService(): iService
	{
		return this.service;
	}
}

export default BaseController;