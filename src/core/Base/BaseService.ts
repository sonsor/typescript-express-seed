import { iService, iDocumentManager, iObject } from 'core/interfaces';
import { Typegoose } from 'typegoose';

/**
 * [iService description]
 * @type {[type]}
 */
abstract class BaseService implements iService
{
	/**
	 * [_dm description]
	 * @type {iDocumentManager}
	 */
	private _dm: iDocumentManager;

	/**
	 * [setDocumentManager description]
	 * @param {iDocumentManager} dm [description]
	 */
	public setDocumentManager(dm: iDocumentManager): void
	{
		this._dm = dm;
	}

	/**
	 * [getDocumentManager description]
	 * @return {iDocumentManager} [description]
	 */
	public getDocumentManager(): iDocumentManager
	{
		return this._dm;
	}

	/**
	 * [find description]
	 * @param  {iObject         = {}}        filters [description]
	 * @param  {iObject         = {}}        fields  [description]
	 * @param  {iObject         = {}}        sort    [description]
	 * @param  {number          = 0}           limit   [description]
	 * @param  {number          = 0}           skip    [description]
	 * @return {Promise<Typegoose>}   [description]
	 */
	public async find(filters: iObject = {}, fields: iObject = {}, sort: iObject = {}, limit: number = 0, skip: number = 0): Promise<Typegoose>
	{
		let query: any = this.getDocumentManager().find(filters);

		if (Object.keys(fields).length > 0) {
			query.select(fields);
		}

		if (Object.keys(sort).length > 0) {
			query.sort(sort);
		}

		if (limit > 0) {
			query.limit(limit);
		}

		if (skip > 0) {
			query.skip(skip);
		}

		return await query.exec();
	}

	/**
	 * [edit description]
	 * @param {iObject} data [description]
	 */
	public edit(data: iObject)
	{

	}

}

export default BaseService;