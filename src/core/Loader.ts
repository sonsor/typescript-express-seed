import { iLoader, iControllerFactory, iServiceFactory, iDocument, iRoute } from 'core/interfaces';
import * as fs from 'fs';
import * as path from 'path';
import { ENV } from 'core';

/**
 * [CoreLoader description]
 * @type {[type]}
 */
class Loader implements iLoader
{
	/**
	 * [CONTROLLERS_PATH description]
	 * @type {string}
	 */
    private CONTROLLERS_PATH: string;

    /**
     * [SERVICES_PATH description]
     * @type {string}
     */
    private SERVICES_PATH: string;

    /**
     * [DOCUMENTS_PATH description]
     * @type {string}
     */
    private DOCUMENTS_PATH: string;

    /**
     * [MIDDILEWARE_PATH description]
     * @type {string}
     */
    private MIDDILEWARE_PATH: string;

    /**
     * [constructor description]
     * @param {any} ENV [description]
     */
    constructor(ENV: any)
    {
        let dir: string = path.resolve(ENV.APP_PATH);
    	this.CONTROLLERS_PATH = path.join(dir, 'Controllers');
    	this.SERVICES_PATH = path.join(dir, 'Services');
    	this.DOCUMENTS_PATH = path.join(dir, 'Documents');
    	this.MIDDILEWARE_PATH = path.join(dir, 'Middlewares');
    }

    /**
     * [load description]
     * @param  {string}             type [description]
     * @param  {string}             name [description]
     * @return {iControllerFactory}      [description]
     */
    public load(type: string, name: string): iControllerFactory & iServiceFactory & iDocument & Function
    {
    	let parts: Array = name.split(path.sep);
    	let fileName: string = parts.pop();
    	let dir: string = parts.join(path.sep);
    	let files: array = [];

        switch (type) {
	        case 'controller':
            case 'C':
	        	dir = path.join(this.CONTROLLERS_PATH, dir);
	        break;
	        case 'service':
            case 'S':
	        	dir = path.join(this.SERVICES_PATH, dir);
	        break;
	        case 'document':
            case 'D':
	        	dir = path.join(this.DOCUMENTS_PATH, dir);
	        break;
	        case 'middleware':
            case 'M':
	        	dir = path.join(this.MIDDILEWARE_PATH, dir);
	        break;
	    }
        
        if (dir) {
        	dir = path.join(dir, path.sep);
        	files = this.getFiles(dir, [], fileName);
        }
        
        if (files.length > 0) {

        	let obj: any = require(files[0]).default;
        	return obj;
        	
        }
        
    }

    /**
     * [getController description]
     * @param  {string} name [description]
     * @return {any}         [description]
     */
    public getController(name: string): iControllerFactory
    {
    	name = this.parse(name, 'C');        
        return this.load('C', name);
    }

    /**
     * [getService description]
     * @param  {string} name [description]
     * @return {any}         [description]
     */
    public getService(name: string): iServiceFactory
    {
    	name = this.parse(name, 'S');
    	return this.load('S', name);
    }

    /**
     * [getDocument description]
     * @param  {string} name [description]
     * @return {any}         [description]
     */
    public getDocument(name: string): iDocument
    {
    	name = this.parse(name, 'D');
    	return this.load('D', name);
    }

    /**
     * [getMiddleware description]
     * @param  {string} name [description]
     * @return {any}         [description]
     */
    public getMiddleware(name: string): Function
    {
    	name = this.parse(name, 'M');
    	return this.load('M', name);
    }

    /**
     * [parse description]
     * @param {string} name [description]
     * @param {string} type [description]
     */
    private parse(name: string, type: string): string
    {
    	name = name.replace('\\', path.sep);
    	switch (type) {
    		case 'controller':
            case 'C':
    			name += 'ControllerFactory.ts';
    		break;
    		case 'service':
            case 'S':
    			name += 'Factory.ts';
    		break;
    		case 'document':
            case 'D':
    			name += 'Document.ts';
    		break;
    		case 'middleware':
            case 'M':
    			name += '.ts';
    		break;
    	}
    	return name;
    }

    /**
     * [getRoutes description]
     * @return {Array<any>} [description]
     */
    public getRoutes(): Array<iRoute>
    {
        let fileName: string = path.resolve(path.join(ENV.CONFIG_DIR, 'routes'));
        let routes = require(fileName).default;
        return routes;
    }

    /**
     * [getFiles description]
     * @param  {string}   dir      [description]
     * @param  {Array}    filelist [description]
     * @param  {string}   fileName [description]
     * @return {Array <any>}                [description]
     */
    private getFiles(dir: string, filelist: Array, fileName: string): Array <string>
    {
        fs.readdirSync(dir).forEach(file => {

            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist = this.getFiles(path.join(dir, file), filelist, fileName);
            } else if (file.toString() == fileName) {
                filelist = filelist.concat(path.join(dir, file));
            }

        });
        return filelist;
    }
}

export default new Loader(ENV);