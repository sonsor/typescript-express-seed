import { iRouter, iRoute, iLoader, iServiceLocator } from 'core/interfaces';
import { Router } from 'express';
import * as path from 'path';
import { Loader } from 'core';
import ServiceLocator from './ServiceLocator';

/**
 * [CoreRouter description]
 * @type {[type]}
 */
class CoreRouter implements iRouter
{
	/**
	 * [routes description]
	 * @type {Array<any>}
	 */
	private routes: Array<iRoute>;

	/**
	 * [router description]
	 * @type {Router}
	 */
	private router: Router;

	/**
	 * [instance description]
	 * @type {CoreRouter}
	 */
	static instance: iRouter;

	/**
	 * [loader description]
	 * @type {iLoader}
	 */
	private loader: iLoader;

	/**
	 * [ServiceLocator description]
	 * @type {iServiceLocator}
	 */
	private ServiceLocator: iServiceLocator;

	/**
	 * [constructor description]
	 * @param {Router}          router         [description]
	 * @param {iServiceLocator} ServiceLocator [description]
	 * @param {iLoader}         Loader         [description]
	 */
	constructor(router: Router, ServiceLocator: iServiceLocator, Loader: iLoader)
	{
		if (CoreRouter.instance) {
			return CoreRouter.instance;
		}
		this.routes = Loader.getRoutes();
		this.router = router;
		this.ServiceLocator = ServiceLocator;
		CoreRouter.instance = this;

		this.loader = Loader;
		this.setup(this.routes);
	}

	/**
	 * [setup description]
	 * @param {Array<iRoute>} routes [description]
	 * @param {string     =      ''}          parent [description]
	 */
	private setup(routes: Array<iRoute>, parent: string = ''): void
	{
		routes.forEach((route) => {

			//this.loader.getController.bind(this.loader);
			let obj: any = this.loader.getController(route.controller).create(this.ServiceLocator);;
			let middlwares: Array<any> = this.middlewares(route);

			if (parent !== '') {
				route.path = (parent !== '/') ? parent + route.path: route.path;
			}

			let action: any = obj[route.action].bind(obj);
			this.addRoute(route.path, route.method, action, middlwares);

			if (route.children && route.children.length > 0) {
				this.setup(route.children, route.path);
			}

		});
	}

	/**
	 * [addRoute description]
	 * @param {string}        path       [description]
	 * @param {string}        method     [description]
	 * @param {string}        action     [description]
	 * @param {Array<string>} middlwares [description]
	 */
	private addRoute(path: string, method: string, action: string, middlwares: Array<string>): void
	{
		let func: any;
		switch (action) {
			case 'post':
				func = this.router.post.bind(this.router);
			break;
			case 'put':
				func = this.router.put.bind(this.router);
			break;
			default:
				func = this.router.get.bind(this.router);
		}

		let params = [path];

		if (middlwares.length > 0) {
			params.push(middlwares);
		}

		params.push(action);
		func.apply(func, params);
	}

	/**
	 * [middlewares description]
	 * @param  {iRoute}          route [description]
	 * @return {Array<Function>}       [description]
	 */
	private middlewares(route: iRoute): Array<Function>
	{
		let callbacks = [];
		if (!('middlewares' in route)) {
			return callbacks;
		}

		if (!(route.middlewares instanceof Array)) {
			route.middlewares = [route.middlewares];
		}

		route.middlewares.forEach((middleware: string) => {

			let func: any = false;

			if (middleware.indexOf('::') > 0) {

				let parts: Array<any> = middleware.split('::');
				let controller: string = parts[0];
				let action: string = parts[1];

				let obj: any = this.loader.getController(controller).create(this.ServiceLocator);;
				func = obj[action].bind(obj);

			} else {

				func = this.loader.getMiddleware(middleware);

			}

			if (func !== false) {
				callbacks.push(func);
			}
		});
		return callbacks;
	}

	/**
	 * [getInstance description]
	 * @return {Router} [description]
	 */
	public getInstance(): Router
	{
		return this.router;
	}
}

let AppRouter: CoreRouter = new CoreRouter(Router(), ServiceLocator, Loader);
export default	AppRouter;