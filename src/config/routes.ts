let routes: Array<any> = [{
	path: '/',
	method: 'get',
	controller: 'Index',
	action: 'index',
	middlewares: [
		'Auth',
		'User\\User::auth'
	],
	children: [{
		path: '/get',
		method: 'get',
		controller: 'User\\User',
		action: 'index',
		children: [],
		middlewares: [
			'Auth',
			'User\\User::auth'
		],
	}, {
		path: '/me',
		method: 'get',
		controller: 'Index',
		action: 'index',
		children: []
	}]
}];

export default routes;