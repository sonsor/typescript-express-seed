import * as path from 'path';

export const APP_DIRNAME = 'application';
export const APP_PATH = path.join(path.dirname(require.main.filename), APP_DIRNAME);
export const CONFIG_DIR = path.join(path.dirname(require.main.filename), 'config');

export const DB_URL = 'mongodb://localhost:27017/test';