const gulp = require('gulp')
const ts = require('gulp-typescript');
const clean = require('gulp-clean');
const NodeUglifier = require('node-uglifier');
const nodemon = require('gulp-nodemon')
let monitor = false;

const src = './tmp/server.js';
const dest = './dist/server.js';

const BUILD_ENV = ((process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development');



const JSON_FILES = ['src/*.json', 'src/**/*.json'];

// pull in the project TypeScript config
const tsProject = ts.createProject('tsconfig.json');


gulp.task('clean', () => {

    return gulp.src(['./tmp/*', './dist/*'], {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});

gulp.task('transpile', ['clean'], () => {

    const tsResult = tsProject.src()
        .pipe(tsProject());
    return tsResult.js.pipe(gulp.dest('tmp'));

});

gulp.task('bundle', ['transpile'], () => {

    if (BUILD_ENV === 'production') {

        const nodeUglifier = new NodeUglifier(src);
        nodeUglifier.merge().uglify();

        nodeUglifier.exportToFile(dest);
        nodeUglifier.exportSourceMaps(dest + '.map');

    }

});

gulp.task('run', ['bundle'], () => {

    gulp.watch('src/**/*.ts', ['bundle']);

});